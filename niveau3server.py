import json
import os
import socketserver
from abc import ABC

class Config(ABC):
    pass

class ConfigInput:
    
    def __init__(self):
        self.nb_players = 0  

    def load_nb_players(self):
        self.nb_players = int(input('Nb players: '))  
        return self.nb_players

    def load_nicknames(self):
        nicknames = []
        for i in range(self.nb_players):  
            nickname = input(f'Nickname for player {i+1}: ')
            nicknames.append(nickname)
        return nicknames

    def load_nb_rows(self):
        return int(input('Nb rows: '))

    def load_nb_cols(self):
        return int(input('Nb cols: '))

    def load_rewards(self):
        nb_rewards = int(input('Nb rewards: '))
        rewards = []
        for _ in range(nb_rewards):
            idx_row = int(input('Index row: '))
            idx_col = int(input('Index col: '))
            rewards.append((idx_row, idx_col))
        return rewards

class ConfigFile:
    
    def __init__(self):
        self.__configfile = 'config.json'
        with open(self.__configfile, 'r') as f:
            self.__configjson = json.load(f)

    def load_nickname(self):
        return self.__configjson['nickname']

    def load_nb_rows(self):
        return self.__configjson['nb_rows']

    def load_nb_cols(self):
        return self.__configjson['nb_cols']

    def load_rewards(self):
        rewards = []
        for reward in self.__configjson['rewards']:
            idx_row = reward['row']
            idx_col = reward['col']
            rewards.append((idx_row, idx_col))
        return rewards

class EmptyCell:
    def __repr__(self):
        return ' '

class Board:

    @classmethod
    def init_board(cls, num_players):
        config = ConfigFile()
        board = Board(config.load_nb_rows(), config.load_nb_cols())
        for position in config.load_rewards():
            board.add_reward(Reward(), position)
        
        if num_players > 0:
            board.add_player(Player(f'Player1'), (0, 0))  
        if num_players > 1:
            board.add_player(Player(f'Player2'), (0, board.nb_cols - 1))  
        if num_players > 2:
            board.add_player(Player(f'Player3'), (board.nb_rows - 1, 0))  
        if num_players > 3:
            board.add_player(Player(f'Player4'), (board.nb_rows - 1, board.nb_cols - 1))  

        return board

    def __init__(self, nb_rows, nb_cols):
        self.__nb_rows = nb_rows
        self.__nb_cols = nb_cols
        self.__board = [[EmptyCell() for _ in range(nb_cols)] for _ in range(nb_rows)]

    def add_player(self, player, position):
        self.__board[position[0]][position[1]] = player

    def add_reward(self, reward, position):
        self.__board[position[0]][position[1]] = reward


    @property
    def nb_rows(self):
        return self.__nb_rows

    @property
    def nb_cols(self):
        return self.__nb_cols



    def find_player_position(self):
        position = ()
        for row_idx, row in enumerate(self.__board):
            for col_idx, cell in enumerate(row):
                if type(cell) == Player:
                    position = (row_idx, col_idx)
        return position

    def move_player(self, direction):
        current_position = self.find_player_position()
        match direction.upper():
            case 'N':
                new_position = (current_position[0] - 1, current_position[1])
            case 'S':
                new_position = (current_position[0] + 1, current_position[1])
            case 'W':
                new_position = (current_position[0], current_position[1] - 1)
            case 'E':
                new_position = (current_position[0], current_position[1] + 1)
            case _:
                return 1
        if new_position[0] < 0 or new_position[0] >= self.__nb_rows or new_position[1] < 0 or new_position[1] >= self.__nb_cols:
            print('Unable to move')
            return 1
        player = self.__board[current_position[0]][current_position[1]]
        self.__board[current_position[0]][current_position[1]] = EmptyCell()
        score = 0
        if type(self.__board[new_position[0]][new_position[1]]) == Reward:
            score = 2
        self.__board[new_position[0]][new_position[1]] = player
        return score

    @property
    def score(self):
        score = 0
        for row in self.__board:
            for cell in row:
                if type(cell) == Reward:
                    score += cell.score
        return score

    def display(self):
        for row in self.__board:
            for cell in row:
                print(f'| {cell} ', end='')
            print('|')

class Player:
    
    def __init__(self, nickname):
        self.__nickname = nickname

    def __repr__(self):
        return 'P'

class Reward:
    
    def __init__(self):
        self.__score = 1

    @property
    def score(self):
        return self.__score

    def __repr__(self):
        return '*'

class Game(socketserver.BaseRequestHandler):

    def __init__(self, request, client_address, server):
        super().__init__(request, client_address, server)
        self.board = None

    def handle(self):
        player_data = self.request.recv(1024).strip()
        if not player_data:
            print("No data received")
            return

        try:
            player_info = json.loads(player_data.decode('ascii'))
            num_players = int(player_info['num_players'])
            print(f"Received {num_players} players")
        except (ValueError, KeyError) as e:
            print(f"Error parsing player data: {e}")
            return

        self.board = Board.init_board(num_players)

        self.request.sendall(bytes(str(1), 'ascii'))

        while True:
            if self.board.score <= 0:
                self.board = Board.init_board(num_players)
                self.request.sendall(bytes(str(3), 'ascii'))
            if self.board.score > 0:
                direction = self.request.recv(1024).strip()
                if not direction:
                    print("No direction received")
                    return

                try:
                    direction_char = chr(direction[0])
                    direction_utf8 = direction.decode("UTF-8")
                    result = self.board.move_player(direction_utf8)
                    self.display()
                    self.request.sendall(bytes(str(result), 'ascii'))
                except (IndexError, ValueError) as e:
                    print(f"Error processing direction: {e}")

    def display(self):
        if self.board:
            self.board.display()


if __name__ == '__main__':
    HOST, PORT = "0.0.0.0", int(os.getenv('PORT'))

    with socketserver.TCPServer((HOST, PORT), Game) as server:
        print('serveur ok')
        server.serve_forever()
        print('after serve_forever')
