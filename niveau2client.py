import socket
import os

class Client:

    def get_direction(self):
        return bytes(input('directio: '), encoding='ascii')

HOST = os.getenv('GAME_HOST')
PORT = int(os.getenv('GAME_PORT'))

if __name__ == '__main__':
    c = Client()
    result = '0'
    while result != '1':
        direction = c.get_direction()
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect((HOST, PORT))
            s.sendall(direction)
            data = s.recv(1024)

        result = str(data)
        print(f"Received {result}")