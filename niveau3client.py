import socket
import os
import json

class Client:
    def get_num_players_and_nicknames(self):
        num_players = input('Number of players: ')
        nicknames = []
        for i in range(int(num_players)):
            nickname = input(f'Nickname for player {i+1}: ')
            nicknames.append(nickname)
        return num_players, nicknames

    def get_direction(self):
        return bytes(input('Direction: '), encoding='ascii')

HOST = os.getenv('GAME_HOST')
PORT = int(os.getenv('GAME_PORT'))

if __name__ == '__main__':
    c = Client()

    num_players, nicknames = c.get_num_players_and_nicknames()
    
    player_data = json.dumps({
        'num_players': num_players,
        'nicknames': nicknames
    })

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((HOST, PORT))
        s.sendall(player_data.encode('ascii'))
        print("Player data sent to the server.")
        
        response = s.recv(1024)
        print(response.decode('ascii'))

    result = '0'
    while result != '1':
        direction = c.get_direction()
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect((HOST, PORT))
            s.sendall(direction)
            data = s.recv(1024)
            result = data.decode('ascii')

        print(f"Received {result}")
