#plateau de jeu l x c 
#récompense avec tuple des loc des récompenses : [(1,1),...]
#position de départ (0,0)

class PlateauDeJeu:
    def __init__(self, largeur, hauteur):
        self.largeur = largeur
        self.hauteur = hauteur
        self.plateau = [[' ' for _ in range(largeur)] for _ in range(hauteur)]
        self.recompenses = {}
        self.position_joueur = (0, 0)
        self.recompenses_trouvees = 0

    def placer_recompenses(self, positions, recompense):
        for position in positions:
            (x, y) = position
            if 0 <= x < self.largeur and 0 <= y < self.hauteur:
                self.recompenses[(x, y)] = recompense
            else:
                print("Position en dehors des limites du plateau.")

    def position_depart(self, position):
        x, y = position
        if 0 <= x < self.largeur and 0 <= y < self.hauteur:
            self.plateau[y][x] = 'P'
            self.position_joueur = position
        else:
            print("Position en dehors des limites du plateau.")

    def avancer_joueur(self, direction):
        x, y = self.position_joueur
        if direction == 'N':
            y -= 1
        elif direction == 'S':
            y += 1
        elif direction == 'W':
            x -= 1
        elif direction == 'E':
            x += 1

        if 0 <= x < self.largeur and 0 <= y < self.hauteur:
            if (x, y) in self.recompenses:
                print("Vous avez trouvé une récompense !")
                self.recompenses_trouvees += 1
                print("Nombre de récompenses trouvées :", plateau.recompenses_trouvees)
                del self.recompenses[(x, y)]
                self.plateau[self.position_joueur[1]][self.position_joueur[0]] = ' '  # Effacer la case précédente
                self.position_joueur = (x, y)
                self.plateau[y][x] = 'P'
            else:
                self.plateau[self.position_joueur[1]][self.position_joueur[0]] = ' '  # Effacer la case précédente
                self.position_joueur = (x, y)
                self.plateau[y][x] = 'P'
        else:
            print("Vous ne pouvez pas sortir du plateau.")

    def afficher_plateau(self):
        for y in range(self.hauteur):
            for x in range(self.largeur):
                print('|', end='')
                contenu_case = self.plateau[y][x]
                if (x, y) in self.recompenses:
                    contenu_case = self.recompenses[(x, y)]
                print(contenu_case, end='')
            print('|')
            print('-' * (self.largeur * 2 - 1))


# Demander la taille du plateau à l'utilisateur
largeur = int(input("Entrez la largeur du plateau : "))
hauteur = int(input("Entrez la hauteur du plateau : "))

# Créer un plateau de jeu avec la taille spécifiée
plateau = PlateauDeJeu(largeur, hauteur)

# Liste des positions des récompenses
recompenses_positions = [(1, 1), (3, 3), (2, 4)]

# Placer les récompenses sur le plateau
plateau.placer_recompenses(recompenses_positions, '*')

plateau.position_depart((0,0))
plateau.afficher_plateau() # Afficher le plateau de jeu

while len(plateau.recompenses) > 0:  # Continuer tant qu'il reste des récompenses sur le plateau
    direction = input("Déplacez-vous avec 'N' (North), 'S' (South), 'W' (West), 'E' (Est) : ")
    if direction in ['N', 'S', 'W', 'E']:
        plateau.avancer_joueur(direction)
        plateau.afficher_plateau()
    else:
        print("Direction invalide. Utilisez 'N', 'S', 'W', ou 'E'.")

print("Félicitations ! Vous avez trouvé toutes les récompenses. Le jeu est terminé.")
print("Vous avez trouvé :", plateau.recompenses_trouvees, "récompense(s)")